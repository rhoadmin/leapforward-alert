/************************************************************************
Royal Hawaiian Orchards
index.js: Main file
************************************************************************/
/* Config file */
const config = require('./config/config');
const twilio = require('twilio');



const main = (async () => {
  const client = new twilio(config.accountSid, config.authToken);
  client.messages
    .create({
       body: 'This is the first msg from the command line!',
       from: config.phone,
       to: '+XXXXXXXXXXX'
     })
    .then(message => console.log(message.sid))
    .done();

});


main();









/* END */

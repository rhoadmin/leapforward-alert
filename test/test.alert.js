/***************************************************************************
Alert Test Suite
5/28/18
The purpose of this test suite is to ensure a well documented and tested
alert service, which can be extended and maintained by whoever takes over the
project.
***************************************************************************/
const { expect } = require('chai');
const should = require('chai').should();
const config = require('../config/config');
const twilio = require('twilio');


describe('Send SMS alerts to users', () => {

  it('should create a client', async () => {
    const client = new twilio(config.accountSid, config.authToken);
    expect(client).to.have.a.property('username');
    expect(client).to.have.a.property('password');
    expect(client).to.have.a.property('accountSid');
  });

});// END describe



/* END */
